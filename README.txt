Mellékeltem a szakmai leirást is:
Megoldásom sima Symfony 4.3 Authentikáció csrf_token -el.

csomagkezelők:
- composer
- npm

konfigurációs beállítások:
.env féjlba és mindenhova a forráskódon belül ahol my_own_grecaptcha_secret_key , ode be kell helyettesíteni a GOOGLE_RECAPTCHA_SECRET_KEY -t
.env féjlba és mindenhova a forráskódon belül ahol my_own_grecaptcha_site_key , ode be kell helyettesíteni a GOOGLE_RECAPTCHA_SITE_KEY -t

.env be és a service.yaml -be:
dbpass helyére beírni a db passwordöt

környezet:
Docker konténer szolgáltatja a környezetet
docker compose fájl mellékeve
docker-compose up -d
login / enter docker:
 docker exec -it icftech-mysql mysql -usf4_user -psf4_pw

Az oldalon a menüpontokkal lehet navigálni
Run build npm
./node_modules/.bin/encore dev
./node_modules/.bin/encore dev --watch

SQL commands:
3 usereel próbáltam ki:

pl:
username: test@test.hu
pass: test

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `login_failure_count`) VALUES
(1, 'test@test.hu', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$KvqyLgfdNe+SBwNl/bWTqA$2hGUaSkM0z3HsrLUWQKLLz1RLiLffNQFYrRPB/o1jt4', 3),
(2, 'edit@edit.hu', '[\"ROLE_EDIT\"]', '$argon2id$v=19$m=65536,t=4,p=1$KvqyLgfdNe+SBwNl/bWTqA$2hGUaSkM0z3HsrLUWQKLLz1RLiLffNQFYrRPB/o1jt4', 0),
(3, 'admin@admin.hu', '[\"ROLE_USER\", \"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$KvqyLgfdNe+SBwNl/bWTqA$2hGUaSkM0z3HsrLUWQKLLz1RLiLffNQFYrRPB/o1jt4', 0);

page
http://localhost:8000

api docs
http://localhost:8000/api/doc

Google Recaptca:Ezeket be kell illeszteni az adott helyre (a sajétomat kitöröltem)
Site key: 6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI
Secret key: 6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe

"With the following test keys, you will always get No CAPTCHA and all verification requests will pass.

Köszönöm a lehetőséget, hogy megoldhattam a tsztfeladatot
Üdvözlettel:Varga Ákos
achozion@gmail.com



