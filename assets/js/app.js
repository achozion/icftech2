/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './assets/css/app.css';
import Cookies from 'js-cookie';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

// loads the jquery package from node_modules
// const jQuery = require('jquery');

var token = Cookies.get('token');

jQuery(document).ready(function () {
    $('.menu-contenteditor').click(function(e){
        e.preventDefault();
        loadPageData("contenteditor/detail");
    });

    $('.menu-user').click(function(e){
        e.preventDefault();
        loadPageData("user/detail");
    });

    $('.menu-admin').click(function(e){
        e.preventDefault();
        loadPageData("admin/detail");
    });

    function putData (url, data) {
        var html = url +' page <br> data: <br> '+ data;
        $('.main-content').html(html);
    }

    jQuery(".form-login").submit(function(e) {
        e.preventDefault();

        login();

        function login () {
            var form = jQuery(this);
            var url = form.attr('action');

            var inputUsername = jQuery("#inputUsername").val();
            var inputPassword = jQuery("#inputPassword").val();
            var param = {
                'username': inputUsername,
                'password': inputPassword
            };
            if (typeof token !== 'undefined'){
                // token = { 'Authorization': 'Bearer ' + Cookies.get('token') };
                token = { 'Authorization': 'Bearer ' + token };
            }
            jQuery.ajax({
                type: "POST",
                contentType: "application/json;charset=utf-8",
                // url: url,
                url: "api/auth/login",
                data: JSON.stringify(param),
                headers: token,
                success: function(data)
                {
                    Cookies.set('token', data.token);
                }
            });
        }
    });

    function loadPageData (url) {
        var token = Cookies.get('token');

        if (typeof token !== 'undefined'){
            token = { 'Authorization': 'Bearer ' + token };
        }

        var param = {
        };

        jQuery.ajax({
            type: "POST",
            contentType: "application/json;charset=utf-8",
            url: "api/"+url,
            data: JSON.stringify(param),
            headers: token,
            success: function(data)
            {
                putData(url, data);
            }
        });
    }

});




