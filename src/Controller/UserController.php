<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends BaseAbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index(Request $request)
    {

        $this->denyAccessUnlessGranted(['ROLE_USER']);

        $data = [
            'data' => 'user'
        ];

        return $this->render('user/index.html.twig', [
            'data' => $data,
        ]);
    }

}