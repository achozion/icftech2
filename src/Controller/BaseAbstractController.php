<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class BaseAbstractController extends AbstractController
{

    protected function getAuthUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }

}
