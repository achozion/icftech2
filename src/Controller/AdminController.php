<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends BaseAbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(Request $request)
    {

        $this->denyAccessUnlessGranted(['ROLE_ADMIN']);

        $data = [
            'data' => 'admin'
        ];

        return $this->render('admin/index.html.twig', [
            'data' => $data,
        ]);
    }

}