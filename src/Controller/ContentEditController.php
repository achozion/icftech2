<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContentEditController extends BaseAbstractController
{
    /**
     * @Route("/contentedit", name="contentedit")
     */
    public function index(Request $request)
    {

        $this->denyAccessUnlessGranted(['ROLE_EDIT']);

        $data = [
            'data' => 'contentedit'
        ];

        return $this->render('contentedit/index.html.twig', [
            'data' => $data,
        ]);
    }

}