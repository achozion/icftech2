<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends BaseAbstractController
{
    /**
     * @Route("/list", name="list")
     */
    public function index(Request $request)
    {
        $data = [
            'Test data 1' => '$1.16 trillion USD',
            'Test data 2' => '$298.3 billion USD',
            'Test data 3' => '$98.5 billion USD',
            'Test data 4' => '$8.2 billion USD',
        ];

        return $this->render('list/index.html.twig', [
            'data' => $data,
        ]);
    }

}